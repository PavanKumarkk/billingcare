/**
 * Created by unni on 2016-05-14.
 */


var mockModule = angular.module("resourceMock", ["ngMockE2E"])

mockModule.run(function ($httpBackend) {
    
    var customerDetailsURL  = new RegExp('/customer/' + "[A-Z|a-z]*[0-9]*" + '/details', '');
    
    var accountSummaryURL = new RegExp('/account/' + "[0-9]*" + '/summary', '');
    var accountBalanceURL = new RegExp('/account/' + "[0-9]*" + '/details/balance', '');
    var accountNotesURL = new RegExp('/account/' + "[0-9]*" + '/details/notes', '');

    var accountConditionsURL = new RegExp('/account/' + "[0-9]*" + '/details/conditions', '');
    var accountTicketsURL = new RegExp('/account/' + "[0-9]*" + '/details/tickets', '');
    var accountBillingdataURL = new RegExp('/account/' + "[0-9]*" + '/details/billing', '');
    var accountHierarchydataURL = new RegExp('/account/' + "[0-9]*" + '/details/hierarchy', '');
    var accountPaymentdataURL = new RegExp('/account/' + "[0-9]*" + '/details/payment', '');

    var servicesSummaryURL = new RegExp('/account/' + "[0-9]*" + '/service/summary', '');
    //\/account\/[0-9]*\/service\/[a-zA-Z0-9_.-]*\/details
    var servicesDetailsURL = new RegExp('/account/' + "[0-9]*" + '/service/' + "[a-zA-Z0-9_.-]*" + '/details')

    var servicesProductDetailsURL = new RegExp('/account/' + "[0-9]*" + '/service/' + "[a-zA-Z0-9_.-]*" + '/product/details')
    var servicesBalanceDetailsURL = new RegExp('/account/' + "[0-9]*" + '/service/' + "[a-zA-Z0-9_.-]*" + '/balance/details')



    $httpBackend.whenGET(servicesSummaryURL).respond(function (method, url, data) {
        console.log('Services Summary URL')
        console.log(url)
        var parameters = url.split('/');
        var accountID = parameters[2]
        var services = null
        console.log(accountID)

        for(i = 0; i < acctServicesSummary.length; i++) {

            if(acctServicesSummary[i].Acctid == accountID) {
                services =  acctServicesSummary[i]
                break

            }
        }
        console.log("Services")
        console.log(services)
        return [200, services, {}];
    })

    $httpBackend.whenGET(servicesDetailsURL).respond(function (method, url, data) {
        console.log('Services Details URL')
        console.log(url)
        var parameters = url.split('/');
        var accountID = parameters[2]
        var serviceID = parameters[4]
        var service = null
        console.log("AccountID:" + accountID)
        console.log("ServiceID:" + serviceID)

        for(i = 0; i < serviceDetailsData.length; i++) {

            if(serviceDetailsData[i].Acctid == accountID) {
                console.log("ServiceDetails Matched")
                console.log(serviceDetailsData[i].Services)
                for(j = 0; j < serviceDetailsData[i].Services.length; j++) {
                    if(serviceDetailsData[i].Services[j].ID == serviceID ) {
                        service =  serviceDetailsData[i].Services[j]
                        break
                    }

                }
                if(service != null)
                    break

            }
        }
        console.log("Service Data")
        console.log(service)
        return [200, service, {}];
    })

    $httpBackend.whenGET(servicesProductDetailsURL).respond(function (method, url, data) {
        console.log('Services Product Details URL')
        console.log(url)
        var parameters = url.split('/');
        var accountID = parameters[2]
        var serviceID = parameters[4]
        var serviceProducts = null
        console.log("AccountID:" + accountID)
        console.log("ServiceID:" + serviceID)

        for(i = 0; i < serviceProductData.length; i++) {

            if(serviceProductData[i].AccountID == accountID && serviceProductData[i].ServiceID == serviceID) {
                console.log("ServiceDetails Matched")
                console.log(serviceProductData[i])
                serviceProducts = serviceProductData[i]
            }
        }
        console.log("Service Product Data")
        console.log(serviceProducts)
        return [200, serviceProducts, {}];
    })

    $httpBackend.whenGET(servicesBalanceDetailsURL).respond(function (method, url, data) {
        console.log('Services Balance Details URL')
        console.log(url)
        var parameters = url.split('/');
        var accountID = parameters[2]
        var serviceID = parameters[4]
        var serviceBalances = null

        console.log("AccountID:" + accountID)
        console.log("ServiceID:" + serviceID)

        for(i = 0; i < serviceBalanceData.length; i++) {
            console.log(serviceBalanceData[i])
            if(serviceBalanceData[i].Acctid == accountID && serviceBalanceData[i].ServiceID == serviceID ) {
                serviceBalances =  serviceBalanceData[i]
                break

            }
        }
        console.log("Service Balance Data")
        console.log(serviceBalances)
        return [200, serviceBalances, {}];
    })

    $httpBackend.whenGET(customerDetailsURL).respond(function (method, url, data) {
        console.log('Customer Details URL')
        console.log(url)
        var parameters = url.split('/');
        var customerID = parameters[2]
        console.log('Customer ID is:',customerID)
        var customer = null
        console.log(customerID)

        for(i = 0; i < CustomerData.length; i++) {

            if(CustomerData[i].Customer.ID == customerID) {
                customer =  CustomerData[i]
                break
            }
        }
        console.log("Customer")
        console.log(customer)
        return [200, customer, {}];
    })

    $httpBackend.whenGET(accountSummaryURL).respond(function (method, url, data) {
        console.log(url)
        var parameters = url.split('/');
        var accountId = parameters[2]
        var account = null
        console.log(accountId)
        for(i = 0; i < AccountData.length; i++) {
            console.log(AccountData[i])
          if(AccountData[i].Account.ID == accountId) {
              account =  AccountData[i]
              break

          }
        }
        console.log("Account")
        console.log(account)
        return [200, account, {}];
    })

    $httpBackend.whenGET(accountBalanceURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var balance = null

        for(i = 0; i < accountBalanceData.length; i++) {
            console.log(AccountData[i])
            if(accountBalanceData[i].Acctid == accountId) {
                balance =  accountBalanceData[i]
                break

            }
        }
        console.log("Balance")
        console.log(balance)
        return [200, balance, {}];
    })

    $httpBackend.whenGET(accountNotesURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var notes = null

        for(i = 0; i < AccountNotesData.length; i++) {
            console.log(AccountNotesData[i])
            if(AccountNotesData[i].Acctid == accountId) {
                notes =  AccountNotesData[i]
                break

            }
        }
        console.log("Notes")
        console.log(notes)
        return [200, notes, {}];
    })

    $httpBackend.whenGET(accountConditionsURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var conditions = null
        //account conditions
        for(i = 0; i < accountConditionsData.length; i++) {
            console.log(accountConditionsData[i])
            if(accountConditionsData[i].Acctid == accountId) {
                conditions =  accountConditionsData[i]
                break

            }
        }
        console.log("conditions")
        console.log(conditions)
        return [200, conditions, {}];
    })

    $httpBackend.whenGET(accountTicketsURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var notes = null

        for(i = 0; i < AccountNotesData.length; i++) {
            console.log(AccountNotesData[i])
            if(AccountNotesData[i].Acctid == accountId) {
                notes =  AccountNotesData[i]
                break

            }
        }
        console.log("Notes")
        console.log(notes)
        return [200, notes, {}];
    })

    $httpBackend.whenGET(accountBillingdataURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var billingData = null

        for(i = 0; i < accountBillingData.length; i++) {
            console.log(accountBillingData[i])
            if(accountBillingData[i].Acctid == accountId) {
                billingData =  accountBillingData[i]
                break

            }
        }
        console.log("billingData")
        console.log(billingData)
        return [200, billingData, {}];
    })

    $httpBackend.whenGET(accountHierarchydataURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var hierarchy = null

        for(i = 0; i < accountHierarchyData.length; i++) {
            console.log(accountHierarchyData[i])
            if(accountHierarchyData[i].AccountID == accountId) {
                hierarchy =  accountHierarchyData[i]
                break

            }
        }
        console.log("hierarchy")
        console.log(hierarchy)
        return [200, hierarchy, {}];
    })

    $httpBackend.whenGET(accountPaymentdataURL).respond(function (method, url, data) {
        var parameters = url.split('/');
        var accountId = parameters[2]
        var payments = null

        for(i = 0; i < accountPaymentData.length; i++) {
            console.log(accountPaymentData[i])
            if(accountPaymentData[i].Acctid == accountId) {
                payments =  accountPaymentData[i]
                break

            }
        }
        console.log("payments")
        console.log(payments)
        return [200, payments, {}];
    })

    // Pass through any requests for application files
    $httpBackend.whenGET(/app/).passThrough();
    $httpBackend.whenGET(/partials/).passThrough();


})