/**
 * Created by unni on 2016-05-18.
 */

var accountPaymentData = [
    {
        Acctid: 10000001,
        BillUnitID: 2001,
        Payments: [{
            PaymentAmount: 10,
            PaymentDate: "10/04/2016",
            PaymentSource: "CreditCard",
            PaymentDescription: "Mastercard 5008",
            AllocatedAmount: 10,
            AllocatedBills:[{
                BillID: 20141
            }]
        }]

    },
    {
        Acctid: 10000002,
        BillUnitID: 2002,
        Payments: [{
            PaymentAmount: 10,
            PaymentDate: "10/04/2016",
            PaymentSource: "CreditCard",
            PaymentDescription: "Mastercard 5008",
            AllocatedAmount: 10,
            AllocatedBills:[{
                BillID: 20151
            }]
        }]
    }

]