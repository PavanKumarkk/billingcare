/**
 * Created by unni on 2016-05-14.
 */



var AccountData = [

    {
        Account: {
            ID: "10000001",
            CustomerIDRef: "C123122",
            Status: "Active",
            Contact:[
                {
                    Type: "Home",
                    FirstName: "John",
                    LastName:  "Doe",
                    Address: "111 Holger Way",
                    County: "Santa Clara",
                    City: "SanJose",
                    State: "California",
                    Country: "USA",
                    Zip: "95134",
                    Email: "John.Doe@email.com",
                    Contact: "212-212-1222"
                }
            ],
            Type:"Consumer",
            SubType: "SMB",
            ServiceCount: 3,
            ActivationDate: "01/01/2016",
            BillUnitCount: 1,
            BillUnit:[ {
                ID: "Primary",
                CycleStartDate: "12/05/2016",
                CycleEndDate: "12/06/2016",
                Currency: "USD",
                PreviousBalance: 0.00,
                CurrentBalance: 100,
                TotalDue: 100,
                Payments: 0,
                DueNow: 100,
                DueDate: "10/05/2016",
                LastPaymentDate: "05/04/2016",
                InCollections: "Yes",
                CollectionsScenario: "Coll-A",
                Frequency: "Monthly"
            }],
            Conditions: {
                CreditClass: "Green"
            },
            Notes:[ {
                ID: 100,
                Type: "Appointment",
                Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
                CreatedTime: "‎Friday‎, ‎Feb‎ ‎1‎, ‎2013‎ ‎11:‎00‎ ‎AM",
                CSRID: "Sam"
            }],
            Tickets:[
                {
                    ID: 20001,
                    Type:"Billing",
                    Subject:"AutoPayment setup",
                    CreatedDate: "5/Mar/16",
                    Status: "Open",
                    LastUpdatedDate: "7/Mar/16",
                    CreatedBy: "Sam",
                    AssignedTo: "Sam"
                },
                {
                    ID: 20002,
                    Type:"Tech Support",
                    Subject:"No Signal",
                    CreatedDate: "25/Mar/16",
                    Status: "Closed",
                    LastUpdatedDate: "25/Mar/16",
                    CreatedBy: "Sam",
                    AssignedTo: "Sam"
                }
            ],
            Services: [
                {
                    Type: "Mobile Service",
                    PrimaryID: ""
                }
            ],
            Analytics: {
                CustomerValue: "$$$"
            }



        }
    },
    {
        Account: {
            ID: "10000002",
            CustomerIDRef: "C123122",
            Status: "Active",
            Contact:[
                {
                    Type: "Home",
                    FirstName: "John",
                    LastName:  "Doe",
                    Address: "111 Holger Way",
                    County: "Santa Clara",
                    City: "SanJose",
                    State: "California",
                    Country: "USA",
                    Zip: "95134",
                    Email: "John.Doe@email.com",
                    Contact: "212-212-1222"
                }
            ],
            Type:"Consumer",
            SubType: "SMB",
            ServiceCount: 3,
            ActivationDate: "01/01/2016",
            BillUnitCount: 1,
            BillUnit: [{
                ID: "Primary",
                CycleStartDate: "12/05/2016",
                CycleEndDate: "12/06/2016",
                Currency: "USD",
                PreviousBalance: 0.00,
                CurrentBalance: 100,
                TotalDue: 100,
                Payments: 0,
                DueNow: 100,
                DueDate: "10/05/2016",
                LastPaymentDate: "05/04/2016",
                InCollections: "Yes",
                CollectionsScenario: "Coll-A",
                Frequency: "Monthly"
            }],
            Conditions: {
                CreditClass: "Green"
            },
            Analytics: {
                CustomerValue: "$$$"
            }



        }
    }




]

var AccountOverviewData =
    {
        Account: {
            ID: "10000001",
            CustomerIDRef: "C123122",
            Status: "Active",
            Contact:[
                {
                    Type: "Home",
                    FirstName: "John",
                    LastName:  "Doe",
                    Address: "111 Holger Way",
                    County: "Santa Clara",
                    City: "SanJose",
                    State: "California",
                    Country: "USA",
                    Zip: "95134",
                    Email: "John.Doe@email.com",
                    Contact: "212-212-1222"
                }
            ],
            Type:"Consumer",
            SubType: "SMB",
            ServiceCount: 3,
            ActivationDate: "01/01/2016",
            BillUnitCount: 1,
            BillUnit:[ {
                ID: "Primary",
                CycleStartDate: "12/05/2016",
                CycleEndDate: "12/06/2016",
                Currency: "USD",
                PreviousBalance: 0.00,
                CurrentBalance: 100,
                TotalDue: 100,
                Payments: 0,
                DueNow: 100,
                DueDate: "10/05/2016",
                LastPaymentDate: "05/04/2016",
                InCollections: "Yes",
                CollectionsScenario: "Coll-A",
                Frequency: "Monthly"
            }],
            Conditions: {
                CreditClass: "Green"
            },
            Notes:[ {
                ID: 100,
               Type: "Appointment",
                Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
                CreatedTime: "‎Friday‎, ‎Feb‎ ‎1‎, ‎2013‎ ‎11:‎00‎ ‎AM",
                CSRID: "Sam"
            }],
            Tickets:[
                {
                        ID: 20001,
                        Type:"Billing",
                        Subject:"AutoPayment setup",
                        CreatedDate: "5/Mar/16",
                        Status: "Open",
                        LastUpdatedDate: "7/Mar/16",
                        CreatedBy: "Sam",
                        AssignedTo: "Sam"
                },
                {
                    ID: 20002,
                    Type:"Tech Support",
                    Subject:"No Signal",
                    CreatedDate: "25/Mar/16",
                    Status: "Closed",
                    LastUpdatedDate: "25/Mar/16",
                    CreatedBy: "Sam",
                    AssignedTo: "Sam"
                }
            ],
            Analytics: {
                CustomerValue: "$$$"
            }



        }
    }
