/**
 * Created by unni on 2016-05-03.
 */

var acctServicesSummaryData =
{
    Acctid: 10000001,
    Services: [
        {
            ID: 41221,
            Type: "Mobile",
            Description: "Mobile Service",
            Status: "Active",
            PrimaryID: "431-222-1222",
            Plan: "Wireless Bundle",
            RemainingContract: "4 months",
            Handset: "iPhone 6",
            CreatedTime: "4/4/2016"
        },
        {
            ID: 41222,
            Type: "Mobile",
            Description: "Mobile Service",
            Status: "Active",
            PrimaryID: "531-222-1322",
            Plan: "Wireless Bundle",
            RemainingContract: "4 months",
            Handset: "Samsung S6",
            CreatedTime: "4/4/2016"
        },
        {
            ID: 41224,
            Type: "Broadband",
            Description: "Broadband Service",
            Status: "Active",
            PrimaryID: "133-122-2221",
            Plan: "Broadband Bundle",
            RemainingContract: "6 months",
            Handset: "Cisco Router",
            CreatedTime: "4/4/2016"
        }
    ]
}


var acctServicesSummary  =
    [
        {
            Acctid: 10000001,
            Services: [
            {
                ID: 41221,
                Type: "Mobile",
                Description: "Mobile Service",
                Status: "Active",
                PrimaryID: "431-222-1222",
                Plan: "Wireless Bundle",
                RemainingContract: "4 months",
                Handset: "iPhone 6",
                CreatedTime: "4/4/2016"
            },
            {
                ID: 41222,
                Type: "Mobile",
                Description: "Mobile Service",
                Status: "Active",
                PrimaryID: "531-222-1322",
                Plan: "Wireless Bundle",
                RemainingContract: "4 months",
                Handset: "Samsung S6",
                CreatedTime: "4/4/2016"
            },
            {
                ID: 41224,
                Type: "Broadband",
                Description: "Broadband Service",
                Status: "Active",
                PrimaryID: "133-122-2221",
                Plan: "Broadband Bundle",
                RemainingContract: "6 months",
                Handset: "Cisco Router",
                CreatedTime: "4/4/2016"
            }]
        },
        {
            Acctid: 10000002,
            Services: [
                {
                    ID: 51221,
                    Type: "Mobile",
                    Description: "Mobile Service",
                    Status: "Active",
                    PrimaryID: "431-222-1222",
                    Plan: "Wireless Bundle",
                    RemainingContract: "4 months",
                    Handset: "iPhone 6",
                    CreatedTime: "4/4/2016"
                },
                {
                    ID: 51222,
                    Type: "Mobile",
                    Description: "Mobile Service",
                    Status: "Active",
                    PrimaryID: "531-222-1322",
                    Plan: "Wireless Bundle",
                    RemainingContract: "4 months",
                    Handset: "Samsung S6",
                    CreatedTime: "4/4/2016"
                },
                {
                    ID: 51224,
                    Type: "Broadband",
                    Description: "Broadband Service",
                    Status: "Active",
                    PrimaryID: "133-122-2221",
                    Plan: "Broadband Bundle",
                    RemainingContract: "6 months",
                    Handset: "Cisco Router",
                    CreatedTime: "4/4/2016"
                }]
        }
    ]


var serviceDetailsData =[
    {
    Acctid: 10000001,
    Services: [
        {
            ID: 41221,
            Type: "Mobile",
            Description: "Mobile Service",
            Status: "Active",
            PrimaryID: "431-222-1222",
            Plan: "Wireless Bundle",
            RemainingContract: "4 months",
            Handset: "iPhone 6",
            CreatedTime: "4/4/2016",
            PrimaryService: "YES",
            SecondaryServiceCount: 0,
            Contract: [{
                ContractTerm: "One Year",
                ContractStartDate: "12/05/2016",
                ContractEndDate: "12/05/2017"
            }],
            Contact: [{
                FirstName: "John",
                LastName: "Doe",
                Address: "255 Burning Grove Pines",
                City: "Douglas City",
                State: "California",
                Zip: "90926-6128",
                Country: "US"
            }],
            ServiceAlerts: [{
                FairUsageLimit: "WithinLimit",
                ActiveTickets: 0,
                PendingServiceOrders: 0,
                FailedServiceOrders: 0
            }],
            Charges: {
                MonthlyCharges: 20,
                UsageCharges: 10,
                OtherCharges: 0
            },
            Devices: [{
                Type: "Phone",
                Properties: [{
                    Name: "IMEI",
                    Value: "***********3542"
                }, {
                    Name: "PUK",
                    Value: "***"
                }, {
                    Name: "Handset",
                    Value: "iPhone 6"
                }
                ]
            }],
            Promotions: [{
                Name: "Add $5LD Plan",
                Type: "Addon"
            }, {
                Name: "Upgrade device $100 off",
                Type: "Discount"
            }]

        }]

},
    {
        Acctid: 10000002,
        Services: [
            {
                ID: 51221,
                Type: "Mobile",
                Description: "Mobile Service",
                Status: "Active",
                PrimaryID: "431-222-1222",
                Plan: "Wireless Bundle",
                RemainingContract: "4 months",
                Handset: "iPhone 6",
                CreatedTime: "4/4/2016",
                PrimaryService: "YES",
                SecondaryServiceCount: 0,
                Contract: [{
                    ContractTerm: "One Year",
                    ContractStartDate: "12/05/2016",
                    ContractEndDate: "12/05/2017"
                }],
                Contact: [{
                    FirstName: "John",
                    LastName: "Doe",
                    Address: "255 Burning Grove Pines",
                    City: "Douglas City",
                    State: "California",
                    Zip: "90926-6128",
                    Country: "US"
                }],
                ServiceAlerts: [{
                    FairUsageLimit: "WithinLimit",
                    ActiveTickets: 0,
                    PendingServiceOrders: 0,
                    FailedServiceOrders: 0
                }],
                Charges: {
                    MonthlyCharges: 20,
                    UsageCharges: 10,
                    OtherCharges: 0
                },
                Devices: [{
                    Type: "Phone",
                    Properties: [{
                        Name: "IMEI",
                        Value: "***********3542"
                    }, {
                        Name: "PUK",
                        Value: "***"
                    }, {
                        Name: "Handset",
                        Value: "iPhone 6"
                    }
                    ]
                }],
                Promotions: [{
                    Name: "Add $5LD Plan",
                    Type: "Addon"
                }, {
                    Name: "Upgrade device $100 off",
                    Type: "Discount"
                },
                    {
                        ID: 51222,
                        Type: "Mobile",
                        Description: "Mobile Service",
                        Status: "Active",
                        PrimaryID: "431-222-1222",
                        Plan: "Wireless Bundle",
                        RemainingContract: "4 months",
                        Handset: "iPhone 6",
                        CreatedTime: "4/4/2016",
                        PrimaryService: "YES",
                        SecondaryServiceCount: 0,
                        Contract: [{
                            ContractTerm: "One Year",
                            ContractStartDate: "12/05/2016",
                            ContractEndDate: "12/05/2017"
                        }],
                        Contact: [{
                            FirstName: "John",
                            LastName: "Doe",
                            Address: "255 Burning Grove Pines",
                            City: "Douglas City",
                            State: "California",
                            Zip: "90926-6128",
                            Country: "US"
                        }],
                        ServiceAlerts: [{
                            FairUsageLimit: "WithinLimit",
                            ActiveTickets: 0,
                            PendingServiceOrders: 0,
                            FailedServiceOrders: 0
                        }],
                        Charges: {
                            MonthlyCharges: 20,
                            UsageCharges: 10,
                            OtherCharges: 0
                        },
                        Devices: [{
                            Type: "Phone",
                            Properties: [{
                                Name: "IMEI",
                                Value: "***********3542"
                            }, {
                                Name: "PUK",
                                Value: "***"
                            }, {
                                Name: "Handset",
                                Value: "iPhone 6"
                            }
                            ]
                        }],
                        Promotions: [{
                            Name: "Add $5LD Plan",
                            Type: "Addon"
                        }, {
                            Name: "Upgrade device $100 off",
                            Type: "Discount"
                        }]

                    }]

            }
        ]
    }
]


