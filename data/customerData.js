/**
 * Created by unni on 2016-05-21.
 */



var CustomerData = [

    {Customer: {
        ID: "C123122",
        Contact: {
            FirstName: "John",
            LastName:  "Doe",
            Address: "111 Holger Way",
            County: "Santa Clara",
            City: "SanJose",
            State: "California",
            Country: "USA",
            Zip: "95134"
        },
        Type: "Individual",
        Status: "Active",
        Account:[ {
            ID: "10000001",
            CustomerIDRef: "C123122",
            Status: "Active",
            Contact: {
                FirstName: "John",
                LastName:  "Doe",
                Address: "111 Holger Way",
                County: "Santa Clara",
                City: "SanJose",
                State: "California",
                Country: "USA",
                Zip: "95134"
            },
            Type:"Consumer",
            SubType: "SMB",
            ServiceCount: 3,
            ActivationDate: "01/01/2016",
            BillUnitCount: 1,
            BillUnit: {
                ID: "Primary",
                CycleStartDate: "",
                CycleEndDate: "",
                Currency: "USD",
                PreviousBalance: 0.00,
                CurrentBalance: 100,
                TotalDue: 100,
                Payments: 0,
                DueNow: 100,
                DueDate: "10/05/2016",
                LastPaymentDate: ""

            }



        }]
    }},
    { Customer: {
        ID: "C123123",
        Contact: {
            FirstName: "Sam",
            LastName:  "Miller",
            Address: "111 Holger Way",
            County: "Santa Clara",
            City: "SanJose",
            State: "California",
            Country: "USA",
            Zip: "95134"
        },
        Type: "Individual",
        Status: "Active",
        Account: [{
            ID: "10000002",
            CustomerIDRef: "C123123",
            Status: "Active",
            Contact: {
                FirstName: "Sam",
                LastName:  "Miller",
                Address: "111 Holger Way",
                County: "Santa Clara",
                City: "SanJose",
                State: "California",
                Country: "USA",
                Zip: "95134"
            },
            Type:"Consumer",
            SubType: "SMB",
            ServiceCount: 3,
            ActivationDate: "01/01/2016",
            BillUnitCount: 1,
            BillUnit: {
                ID: "Primary",
                CycleStartDate: "",
                CycleEndDate: "",
                Currency: "USD",
                PreviousBalance: 0.00,
                CurrentBalance: 100,
                TotalDue: 100,
                Payments: 0,
                DueNow: 100,
                DueDate: "10/05/2016",
                LastPaymentDate: ""

            }



        }]
    }}



]