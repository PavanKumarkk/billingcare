/**
 * Created by unni on 2016-05-18.
 */



var accountHierarchyData = [
    {
        AccountID: 10000001,
        AccountNumber: "444233411",
        Contact: {
            FirstName: "John",
            LastName: "Peter"
        },
        Services: 4,
        PaidBy: "Self",
        Description:"Paying Account",
        items:[
            {
                AccountID: 124412,
                AccountNumber: "1233411",
                PaidBy: "Parent",
                Description:"Non-Paying Account",
                Services: 2,
                Contact: {
                    FirstName: "Sam",
                    LastName: "Peter"
                }

            },
            {
                AccountID: 124412,
                AccountNumber: "1233411",
                PaidBy: "Self",
                Description:"Paying Account",
                Services: 2,
                Contact: {
                    FirstName: "Scott",
                    LastName: "Thomson"
                },
                items:[
                    {
                        AccountID: 224414,
                        AccountNumber: "2233411",
                        PaidBy: "Parent",
                        Description:"Non-Paying Account",
                        Services: 2,
                        Contact: {
                            FirstName: "Sam",
                            LastName: "Peter"
                        }

                    },
                    {
                        AccountID: 224412,
                        PaidBy: "Parent",
                        AccountNumber: "2233411",
                        Services: 2,
                        Contact: {
                            FirstName: "Scott",
                            LastName: "Thomson"
                        }

                    },
                ]

            }
        ]
    }
]


