/**
 * Created by unni on 2016-05-18.
 */


var AccountNotesData = [
    {
        Acctid: 10000001,
        Notes: [{
            ID: 100,
            Type: "Appointment",
            Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
            CreatedTime: "‎2015-05-18",
            CSRID: "Sam"
        },
            {
                ID: 101,
                Type: "Appointment",
                Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
                CreatedTime: "‎2015-05-19 ",
                CSRID: "Sam"
            }
        ]
    },
    {
        Acctid: 10000002,
        Notes: [{
            ID: 104,
            Type: "Appointment",
            Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
            CreatedTime: "‎2015-05-18",
            CSRID: "Sam"
        },
            {
                ID: 105,
                Type: "Appointment",
                Text: "Phone Swap at Dealer location NorthLand Mall on 5th Feb",
                CreatedTime: "‎2015-05-19",
                CSRID: "Sam"
            }
        ]
    }
]
