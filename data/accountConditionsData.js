/**
 * Created by unni on 2016-05-18.
 */
var accountConditionsData = [
    {
        Acctid: 10000001,
        Conditions: {
            CreditClass: "Green",
            CreditLimit: 600,
            MaxServices: 3,
            MaxSecondaryServices: 6,
            MaxSecondaryServicePerPrimary: 2,
            ServiceLimits: [
                {
                    Type: "LocalData",
                    Limit: "2GB"
                },
                {
                    Type: "NAMRoamingData",
                    Limit: "500MB"
                },
                {
                    Type: "RoamingData",
                    Limit: "50MB"
                },
                {
                    Type: "ServiceCreditLimit",
                    Limit: "100"
                }
            ],
            MissedPayments: 1,
            ConsecutivePayments: 2,
            Notifications: {
                CreditLimitThreshold: "90",
                DataLimitThreshold: 80
            }
        }
    },
    {
        Acctid: 10000002,
        Conditions: {
            CreditClass: "Green",
            CreditLimit: 600,
            MaxServices: 3,
            MaxSecondaryServices: 6,
            MaxSecondaryServicePerPrimary: 2,
            ServiceLimits: [
                {
                    Type: "LocalData",
                    Limit: "2GB"
                },
                {
                    Type: "NAMRoamingData",
                    Limit: "500MB"
                },
                {
                    Type: "RoamingData",
                    Limit: "50MB"
                },
                {
                    Type: "ServiceCreditLimit",
                    Limit: "100"
                }
            ],
            MissedPayments: 1,
            ConsecutivePayments: 2,
            Notifications: {
                CreditLimitThreshold: "90",
                DataLimitThreshold: 80
            }
        }
    }
]
