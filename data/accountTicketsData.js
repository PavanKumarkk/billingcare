/**
 * Created by unni on 2016-05-18.
 */
var AccountTicketsData = [
    {
        Acctid: 10000001,
        Tickets: [
            {
                ID: 1001,
                Type: "Billing",
                Subject: "Autopayment setup",
                CreatedTime: "‎2015-05-16",
                LastUpdatedTime: "‎2015-05-16",
                Status: "Open",
                CSRID: "Sam",
                Details: "",
                DetailLnk: ""
            },
            {
                ID: 1002,
                Type: "TechSupport",
                Subject: "No Signal",
                CreatedTime: "‎2015-05-16",
                LastUpdatedTime: "‎2015-05-16",
                Status: "Open",
                CSRID: "Sam",
                Details: "",
                DetailLnk: ""
            }
    ]
},
    {
        Acctid: 10000002,
        Tickets: [
            {
                ID: 2001,
                Type: "Billing",
                Subject: "Autopayment setup",
                CreatedTime: "‎2015-05-16",
                LastUpdatedTime: "‎2015-05-16",
                Status: "Open",
                CSRID: "Sam",
                Details: "",
                DetailLnk: ""
            },
            {
                ID: 2002,
                Type: "TechSupport",
                Subject: "No Signal",
                CreatedTime: "‎2015-05-16",
                LastUpdatedTime: "‎2015-05-16",
                Status: "Open",
                CSRID: "Sam",
                Details: "",
                DetailLnk: ""
            }
        ]
    }
]
