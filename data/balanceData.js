/**
 * Created by unni on 2016-05-17.
 */
var accountBalanceData = [
    {
        Acctid: 10000001,
        Balances:
        [
            {
                ID: 10001,
                Description: "Weekday Shared Minutes",
                Granted: 1000,
                Remaining:500,
                UOM: "Minutes",
                ValidFrom: "12/05/2016",
                ValidTo: "12/06/2016",
                RollOver: "None"
            },
            {
                ID: 100012,
                Description: "National Shared Data",
                Granted: 2048,
                Remaining: 700,
                UOM: "Gibibyte",
                ValidFrom: "12/05/2016",
                ValidTo: "12/06/2016",
                RollOver: "None"
            }
        ]
    },
    {
        Acctid: 10000002,
        Balances:
            [
                {
                    ID: 100012,
                    Description: "National Shared Data",
                    Granted: 2048,
                    Remaining: 700,
                    UOM: "Gibibyte",
                    ValidFrom: "12/05/2016",
                    ValidTo: "12/06/2016",
                    RollOver: "None"
                }
            ]
    }
]

var serviceBalanceData = [
    {
        Acctid: 10000001,
        ServiceID:41221,
        Balances:
            [
                {
                    ID: 10001,
                    Description: "Weekday Minutes",
                    Granted: 1000,
                    Remaining:500,
                    UOM: "Minutes",
                    ValidFrom: "12/05/2016",
                    ValidTo: "12/06/2016",
                    RollOver: "None"
                },
                {
                    ID: 100012,
                    Description: "National Data",
                    Granted: 2048,
                    Remaining: 700,
                    UOM: "Gibibyte",
                    ValidFrom: "12/05/2016",
                    ValidTo: "12/06/2016",
                    RollOver: "None"
                }
            ]
    },
    {
        Acctid: 10000002,
        ServiceID:51221,
        Balances:
            [
                {
                    ID: 100012,
                    Description: "National Data",
                    Granted: 2048,
                    Remaining: 700,
                    UOM: "Gibibyte",
                    ValidFrom: "12/05/2016",
                    ValidTo: "12/06/2016",
                    RollOver: "None"
                }
            ]
    }
]