 
var accountBillingData = [
    {
        Acctid: 10000001,
        BillUnit:[ {
            ID: "Primary",
            CycleStartDate: "12/05/2016",
            CycleEndDate: "12/06/2016",
            Currency: "USD",
            PreviousBalance: 0.00,
            CurrentBalance: 100,
            DOM: 12,
            Segment: "default",
            TotalDue: 100,
            Payments: 0,
            DueNow: 100,
            DueDate: "10/05/2016",
            LastPaymentDate: "05/04/2016",
            InCollections: "Yes",
            CollectionsScenario: "Coll-A",
            Frequency: "Monthly",
            PreAuth: "None",
            Bills: [{
                ID: "b1-1220",
                Start: "12/05/2016",
                End: "12/06/2016",
                PreviousBalance:100,
                CurrentTotal: 20,
                TotalBalance: 120,
                Status: "UnBilled",
                DueDate: "12/06/2016",
                InvoiceLink: "NA"


            },
                {
                    ID: "b1-1221",
                    Start: "12/04/2016",
                    End: "12/05/2016",
                    PreviousBalance:0,
                    CurrentTotal: 100,
                    TotalBalance: 100,
                    Status: "Billed",
                    DueDate: "12/06/2016",
                    InvoiceLink: "/invoice/b1-1221.pdf"
                }]
        }]
    },
    {
        Acctid: 10000002,
        BillUnit:[ {
            ID: "Primary",
            CycleStartDate: "12/05/2016",
            CycleEndDate: "12/06/2016",
            Currency: "USD",
            PreviousBalance: 0.00,
            CurrentBalance: 100,
            DOM: 12,
            Segment: "default",
            TotalDue: 100,
            Payments: 0,
            DueNow: 100,
            DueDate: "10/05/2016",
            LastPaymentDate: "05/04/2016",
            InCollections: "Yes",
            CollectionsScenario: "Coll-A",
            Frequency: "Monthly",
            PreAuth: "CreditCard",
            PreAuthDetails: [{
                Type: "CreditCard",
                BankAccountNo: "1520",
                CardType: "MasterCard",
                CardExpiry:"05/17"
            }],
            Bills: [{
                ID: "b1-1222",
                Start: "12/05/2016",
                End: "12/06/2016",
                PreviousBalance:100,
                CurrentTotal: 20,
                TotalBalance: 120,
                Status: "UnBilled",
                DueDate: "12/06/2016",
                InvoiceLink: "NA"

            },
                {
                    ID: "b1-1223",
                    Start: "12/04/2016",
                    End: "12/05/2016",
                    PreviousBalance:0,
                    CurrentTotal: 100,
                    TotalBalance: 100,
                    Status: "Billed",
                    DueDate: "12/06/2016",
                    InvoiceLink: "/invoice/b1-1223.pdf"
                }]
        }]
    }

]