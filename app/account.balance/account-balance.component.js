/**
 * Created by unni on 2016-05-17.
 */


(function() {
    'use strict';
    angular.module('app.account').component("accountBalance", {
        templateUrl: "app/account.balance/account-balance.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this
            console.log("BalanceController")
            console.log(vm.accountData)
            vm.$onInit = function() {
                dataservice.getBalanceData(vm.accountData.Account.ID).then(function(balances){
                    console.log("GetBalanceData")
                    console.log(balances)
                    vm.balanceGridOptions = {
                        dataSource: {
                            data: balances.Balances,
                            schema: {
                                model: {
                                    fields: {
                                        ID: { type: "string" },
                                        Description:{ type: "string" },
                                        Granted: { type: "string" },
                                        Remaining: { type: "string" },
                                        UOM: { type: "string" },
                                        ValidFrom: { type: "string" },
                                        ValidTo: { type: "string" },
                                        RollOver: { type: "string" }
                                    }
                                }
                            },
                            pageSize: 20
                        },
                        height: 250,
                        scrollable: true,
                        sortable: true,
                        resizable: true,
                        columnMenu: true,
                        groupable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
                        columns: [


                            { field: "Description", title: "Balance"  },
                            { field: "Granted", title: "Granted"  },
                            { field: "Remaining", title: "Remaining"  },
                            { field: "UOM", title: "UOM" },
                            { field: "ValidTo", title: "ExpiresOn" }

                        ]
                    };
                })
            }

        }

    })
})();