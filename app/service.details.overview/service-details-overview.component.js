

(function () {
    'use strict';
    angular.module('app.service').component("serviceDetailsOverview", {
        templateUrl: "app/service.details.overview/service-details-overview.view.html",
        bindings: {
            serviceDetailsData : '<'
        },
        controllerAs: "model",
        controller: function (dataservice) {
            var vm = this


            //initialize static tag
            vm.$onInit = function() {

            }
        }


    })
})();