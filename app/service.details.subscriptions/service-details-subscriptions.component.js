/**
 * Created by unni on 2016-05-24.
 */
/**
 * Created by unni on 2016-05-24.
 */


(function () {
    'use strict';
    angular.module('app.service').component("serviceDetailSubscriptions", {
        templateUrl: "app/service.details.subscriptions/service-details-subscriptions.view.html",
        bindings: {
            accountId : '@',
            serviceId: '@'
        },
        controllerAs: "model",
        controller: function (dataservice, $scope) {
            var vm = this

            vm.$onInit = function() {
                dataservice.getServiceProductsData(vm.accountId, vm.serviceId).then(function(data) {
                    vm.serviceProductsData = data


                    $scope.productGridOptions = {
                        dataSource: {
                            data: productData,
                            schema: {
                                model: {
                                    fields: {
                                        PlanName:{ type: "string" },
                                        BundleName: { type: "string" },
                                        ProductName: { type: "string" },
                                        RecurringFee: { type: "number" },
                                        OnetimeFee: { type: "number" },
                                        Status: { type: "string" },
                                        PurchaseStartTime: { type: "string" },
                                        PurchaseEndTime: { type: "string" }
                                    }
                                }
                            },
                            pageSize: 20
                        },
                        height: 250,
                        scrollable: true,
                        sortable: true,
                        resizable: true,
                        columnMenu: true,
                        groupable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
                        columns: [

                            { field: "PlanName", title: "Plan"  },
                            { field: "BundleName", title: "Bundle"  },
                            { field: "ProductName", title: "Product"  },
                            { field: "RecurringFee", title: "RC", format: "{0:c}"  },
                            { field: "OnetimeFee", title: "OC", format: "{0:c}"  },
                            { field: "Status", title: "Status" },
                            { field: "PurchaseStartTime",title: "PurchaseStart"  },
                            { field: "PurchaseEndTime",title: "PurchaseEnd" }
                        ]
                    };

                    $scope.productGridAddonOptions = {
                        dataSource: {
                            data: productAddonData,
                            schema: {
                                model: {
                                    fields: {

                                        BundleName: { type: "string" },
                                        ProductName: { type: "string" },
                                        RecurringFee: { type: "number" },
                                        OnetimeFee: { type: "number" },
                                        Status: { type: "string" },
                                        PurchaseStartTime: { type: "string" },
                                        PurchaseEndTime: { type: "string" }
                                    }
                                }
                            },
                            pageSize: 20
                        },
                        height: 250,
                        scrollable: true,
                        sortable: true,
                        resizable: true,
                        columnMenu: true,
                        groupable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
                        columns: [


                            { field: "BundleName", title: "Bundle"  },
                            { field: "ProductName", title: "Product"  },
                            { field: "RecurringFee", title: "RC", format: "{0:c}"  },
                            { field: "OnetimeFee", title: "OC", format: "{0:c}"  },
                            { field: "Status", title: "Status" },
                            { field: "PurchaseStartTime",title: "PurchaseStart"  },
                            { field: "PurchaseEndTime",title: "PurchaseEnd" }
                        ]
                    };



                }, function(data) {
                    console.log("Failed " + data)
                })
            }

    
        }


    })
})();