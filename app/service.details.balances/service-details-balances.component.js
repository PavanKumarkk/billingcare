/**
 * Created by unni on 2016-05-24.
 */


(function () {
    'use strict';
    angular.module('app.service').component("serviceDetailBalances", {
        templateUrl: "app/service.details.balances/service-details-balances.view.html",
        bindings: {
            accountId : '@',
            serviceId: '@'
        },
        controllerAs: "model",
        controller: function (dataservice) {
            var vm = this

            vm.$onInit = function() {
                dataservice.getServiceBalanceData(vm.accountId, vm.serviceId).then(function(data) {
                    vm.serviceBalancesData = data

                    vm.balanceGridOptions = {
                        dataSource: {
                            data: vm.serviceBalancesData.Balances,
                            schema: {
                                model: {
                                    fields: {
                                        ID: { type: "string" },
                                        Description:{ type: "string" },
                                        Granted: { type: "string" },
                                        Remaining: { type: "string" },
                                        UOM: { type: "string" },
                                        ValidFrom: { type: "string" },
                                        ValidTo: { type: "string" },
                                        RollOver: { type: "string" }
                                    }
                                }
                            },
                            pageSize: 20
                        },
                        height: 250,
                        scrollable: true,
                        sortable: true,
                        resizable: true,
                        columnMenu: true,
                        groupable: true,
                        pageable: {
                            input: true,
                            numeric: false
                        },
                        columns: [


                            { field: "Description", title: "Balance"  },
                            { field: "Granted", title: "Granted"  },
                            { field: "Remaining", title: "Remaining"  },
                            { field: "UOM", title: "UOM" },
                            { field: "ValidTo", title: "ExpiresOn" }

                        ]
                    };
                }, function(data) {
                    console.log("Failed " + data)
                })
            }

          
        }


    })
})();