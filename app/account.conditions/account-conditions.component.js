/**
 * Created by unni on 2016-05-18.
 */


(function() {
    'use strict';
    angular.module('app.account').component("accountConditions", {
        templateUrl: "app/account.conditions/account-conditions.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this
            vm.$onInit = function() {
                dataservice.getConditionsData(vm.accountData.Account.ID).then(function(data) {
                    console.log("getConditionsData")
                    console.log(data)
                    //loop through billing data, set biling Grid Options
                    vm.conditionsData = data
                    //vm.billingGridOptions = billingGridOptions

                })
            }
        }
    })
})();

