


(function() {
    'use strict';
    angular.module('app.account').component("accountBilling", {
        templateUrl: "app/account.billing/account-billing.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this

            vm.$onInit = function() {
                 

               dataservice.getBillingData(vm.accountData.Account.ID).then(function(data) {
                   console.log("GetBillingData")
                   console.log(data)
                   //loop through billing data, set biling Grid Options
                   vm.billingData = data
                   //vm.billingGridOptions = billingGridOptions

               })
            }
        }
    })
})();