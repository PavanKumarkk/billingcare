/**
 * Created by unni on 2016-05-17.
 */


(function() {
    'use strict';
    angular.module('app.account').component("accountNote", {
        templateUrl: "app/account.note/account-note.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this
            vm.$onInit = function() {
                dataservice.getNotesData(vm.accountData.Account.ID).then(function(notes){
                    console.log("Notes")
                    console.log(notes)
                    vm.notes = notes
                })
            }
        }
    })
})();