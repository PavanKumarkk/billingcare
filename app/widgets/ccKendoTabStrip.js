
(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('kendoTabStrip', ccKendoTabStrip);

//still need to understand why setting a priority allows us to access the element

    /* @ngInject */
    function ccKendoTabStrip () {

        var directive = {

            require: [
                '?kendoTabStrip'
            ],
            //important..if no priority set, element is not accessible
            priority: 1,
            link: linkFn

        };
        return directive;

       function linkFn(scope, element, attributes) {
            console.log('post')
            console.log("Scope")
            console.log(scope)

            var aElem = angular.element(element);
            if(scope.tabs!= null) {
                scope.tabs.forEach(function(tabData, index, array){
                    console.log(element)
                    if(tabData["closeBtn"]) {
                       
                        console.log("Close Button requested")
                        var target = $(element[0]).find('.k-tabstrip-items > .k-item > .k-link')
                        console.log($(target[index]))
                        $(target[index]).append("<button data-type='remove'>X</button>")

                    }
                })

                aElem.data("kendoTabStrip").select(0)
            }
            aElem.on("click", "[data-type='remove']", function (e) {
                e.preventDefault();
                e.stopPropagation();
                var item = $(e.target).closest(".k-item");
                console.log(item.index())
                scope.$apply(function(){

                    if(scope.tabs != null)
                        scope.tabs.splice(item.index(), 1)
                    else {
                        //for cases where we dont have a datasource..ie when we use append
                        aElem.data("kendoTabStrip").remove(item.index());
                        aElem.data("kendoTabStrip").select(0)
                    }
                })
            });


        }
    }
})();

