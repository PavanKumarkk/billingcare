/**
 * Created by unni on 2016-05-16.
 */
(function() {
    'use strict';

    angular
        .module('app.widgets')
        .directive('ccNavbar', ccNavbar);

    /* @ngInject */
    function ccNavbar () {

        var directive = {

            restrict: 'EA',
            templateUrl:'partials_tuts1/navbar.html',
            controller: ccNavbarCtrl

        };
        return directive;

        function ccNavbarCtrl($scope, $location) {
            $scope.isActive = function(path) {

            //    console.log($location.path())

                var currentPath = $location.path().split('/')[1];

              //  console.log(currentPath)

                if (currentPath.indexOf('?') !== -1) {
                    currentPath = currentPath.split('?')[0];
                  

                }

                return currentPath === path.split('/')[1];
            }
        }
    }
})();