/**
 * Created by unni on 2016-05-14.
 */
(function() {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

   

    function dataservice($http, $location, $q, Account, Balance) {
        var service = {
            getCustomerData: getCustomerData,
            getAccountData: getAccountData,
            getBalanceData: getBalanceData,
            getNotesData: getNotesData,
            getBillingData: getBillingData,
            getPaymentData: getPaymentData,
            getHierarchyData: getHierarchyData,
            getConditionsData: getConditionsData,
            getServiceSummaryData: getServiceSummaryData,
            getServiceDetailsData: getServiceDetailsData,
            getServiceProductsData: getServiceProductsData,
            getServiceBalanceData: getServiceBalanceData

        };

        return service;
        //TODO = Move the below function to models


        function getServiceProductsData(accountID, serviceID) {
            var url =  '/account/' + accountID + '/service/' + serviceID + '/product/details'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getServiceProductsComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getServiceProductsComplete(data, status, headers, config) {
                console.log("getServiceProductsComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

        function getServiceBalanceData(accountID, serviceID) {
            var url =  '/account/' + accountID + '/service/' + serviceID + '/balance/details'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getServiceBalanceComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getServiceBalanceComplete(data, status, headers, config) {
                console.log("getServiceBalanceComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

        function getServiceDetailsData(accountID, serviceID) {
            var url =  '/account/' + accountID + '/service/' + serviceID + '/details'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getServiceComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getServiceComplete(data, status, headers, config) {
                console.log("getServiceComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }


        function getCustomerData(customerID) {
            var url = '/customer/' + customerID+ '/details'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getCustomerComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getCustomerComplete(data, status, headers, config) {
                console.log("getCustomerComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

        function getBillingData(accountID) {
            var url = '/account/' + accountID + '/details/billing'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getBillingComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getBillingComplete(data, status, headers, config) {
                console.log("getBillingComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

        function getPaymentData(accountID) {
            var url = '/account/' + accountID + '/details/payment'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getPaymentComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getPaymentComplete(data, status, headers, config) {
                console.log("getPaymentComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

        function getHierarchyData(accountID) {
            var url = '/account/' + accountID + '/details/hierarchy'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getHierarchyComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getHierarchyComplete(data, status, headers, config) {
                console.log("getHierarchyComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }
        function getConditionsData(accountID) {
            var url = '/account/' + accountID + '/details/conditions'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getConditionsComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getConditionsComplete(data, status, headers, config) {
                console.log("getConditionsComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }



        function getAccountData(accountid) {
            var url = '/account/' + accountid + '/summary'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getAccountsComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });

            function getAccountsComplete(data, status, headers, config) {
                return Account.new(data.data)
               // return data.data;
            }
        }
        function getAccountsComplete(data, status, headers, config) {
            return Account.new(data.data)
            // return data.data;
        }

        function getBalanceData(accountid) {
            var url = '/account/' + accountid+ '/details/balance'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getBalanceComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });

            function getAccountsComplete(data, status, headers, config) {
                return Account.new(data.data)
                // return data.data;
            }
            function getBalanceComplete(data, status, headers, config) {
                console.log("getBalanceComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }
        function getBalanceComplete(data, status, headers, config) {
            console.log("getBalanceComplete")
            console.log(data.data)
            return data.data
            // return data.data;
        }
        function getNotesData(accountid) {
            var url = '/account/' + accountid+ '/details/notes'
            console.log('URL:' + url)
            return $http.get(url)
                .then(getNotesComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });


            function getNotesComplete(data, status, headers, config) {
                console.log("getNotesComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }

        }


        function getServiceSummaryData(accountID) {
            var url = '/account/' + accountID+ '/service/summary'

            console.log('URL:' + url)
            return $http.get(url)
                .then(getServiceComplete)
                .catch(function(message) {
                    console.log('failed')
                    //    exception.catcher('XHR Failed for getCustomers')(message);
                    $location.url('/');
                });
            function getServiceComplete(data, status, headers, config) {
                console.log("getServiceComplete")
                console.log(data.data)
                return data.data
                // return data.data;
            }
        }

    }

    angular
        .module('app.core')
        .factory('Account', function() {
            var Account = function Account(data) {
                if(data) {
                    this.setData(data)
                }
                return this
            }
            Account.prototype = {
                setData: function(accountData) {
                    angular.extend(this, accountData)
                }

            }
            return {
                new: function(data) {
                    return new Account(data)
                }
            }
        })

    angular
        .module('app.core')
        .factory('Balance', function() {
            var Balance = function Balance(data) {
                if(data) {
                    this.setData(data)
                }
                return this
            }
            Balance.prototype = {
                setData: function(balanceData) {
                    console.log("Balance Prototype")
                    console.log(balanceData)
                    angular.extend(this, balanceData)
                }

            }
            return {
                new: function(data) {
                    return new Balance(data)
                }
            }
        })

})();