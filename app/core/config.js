/**
 * Created by unni on 2016-05-16.
 */
(function() {
    'use strict';

    var core = angular.module('app.core');
    core.value("$routerRootComponent", "billingCare")
    core.run(function($rootScope, $location){
        $rootScope.host = $location.host();
    });

})();