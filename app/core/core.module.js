/**
 * Created by unni on 2016-05-14.
 */

(function() {
    'use strict';

    angular.module('app.core', [
        'ui.bootstrap', 'kendo.directives', "ngComponentRouter"

    ]);
    
})();
