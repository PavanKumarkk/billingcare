/**
 * Created by unni on 2016-05-16.
 */
(function() {
    'use strict';
    angular.module('app.search').component("search", {
        templateUrl: "app/search/search.html",
        bindings: {
            openAccountTab : '&'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this

            vm.searchResults= [
                {
                    CustomerID: "C123122",
                    AccountNo: "12333",
                    ServiceID: "34412",
                    FirstName: "Sam",
                    LastName: "Sam"
                },
                {
                    CustomerID: "C123123",
                    AccountNo: "12333",
                    ServiceID: "34412",
                    FirstName: "Sam",
                    LastName: "Sam"
                },
                {
                    CustomerID: "12233",
                    AccountNo: "12333",
                    ServiceID: "34412",
                    FirstName: "Sam",
                    LastName: "Sam"
                }
            ]

            vm.searchOptions = {
                dataSource: {
                    data: vm.searchResults,
                    schema: {
                        model: {
                            fields: {
                                CustomerID: { type: "string" },
                                AccountNo:{ type: "string" },
                                ServiceID: {type:"string"},
                                FirstName: { type: "string" },
                                LastName: { type: "string" }

                            }
                        }
                    },
                    pageSize: 20
                },
                height: 250,
                scrollable: true,
                sortable: true,
                resizable: true,
                columnMenu: true,

                pageable: {
                    input: true,
                    numeric: false
                },
                columns: [


                    { field: "CustomerID", title: "CustomerID" ,  template: '<a href="\\#/customer/#:CustomerID#">#=CustomerID#</a>' },
                    { field: "AccountNo", title: "AccountNo",  },
                    { field: "ServiceID", title: "ServiceID"  },
                    { field: "FirstName", title: "FirstName" },
                    { field: "LastName", title: "LastName" }

                ]
            };

            vm.searchID = "10000002"
            vm.searchMessage = ""
            vm.searchAccounts = function() {
                if(vm.searchID == "") {
                    alert("Enter search id")
                    vm.searchMessage = ""
                } else {
                    vm.searchMessage = "Searching for account id " + vm.searchID + " ....."
                    dataservice.getAccountData(vm.searchID).then(
                        function(data) {
                        console.log("Account Data")
                        console.log(data)
                            console.log(this)
                            console.log(vm)
                            vm.searchMessage = data
                            vm.openAccountTab({accountData: data})
                    }, function(data) {
                        alert('failed')
                        })
                    
                }
            }
        }


    })
})();