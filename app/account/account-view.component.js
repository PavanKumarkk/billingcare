/**
 * Created by unni on 2016-05-15.
 */

(function() {
    'use strict';
    angular.module('app.account').component("account", {
        templateUrl: "app/account/accountview.html",
        bindings: {
            accountData: "<"
        },
        controllerAs: "model",
        controller: function() {
            var vm = this
            console.log("Account Controller")
            console.log(vm.accountData)
            vm.testData = {
                Name: "Same"
            }
            vm.updateContact = function(newContact) {

                console.log(newContact)
                //create a copy of data ..deep copy
                vm.accountData.Account.Contact = angular.copy(newContact)
            }
        }
        
    })
})();