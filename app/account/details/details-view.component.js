/**
 * Created by unni on 2016-05-17.
 */


(function() {
    'use strict';
    angular.module('app.account').component("accountDetails", {
        templateUrl: "app/account/details/accountdetails.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function() {
            var vm = this
            vm.tabStripOptions= {
                scrollable: false,
                tabPosition: "top",
                
            };

        }

    })
})();