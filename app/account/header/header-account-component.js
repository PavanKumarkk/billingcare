/**
 * Created by unni on 2016-05-15.
 */

(function() {
    'use strict';
    angular.module('app.account').component("headerAccount", {
        templateUrl: "app/account/header/header-account.html",
        bindings: {
            headerAcctData: '<'
        },
        controllerAs: "model",
        controller: function() {
            console.log("Header controller")
            var vm = this
            console.log(vm.headerAcctData)
        }

    })
})();