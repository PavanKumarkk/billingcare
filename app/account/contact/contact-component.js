(function() {
    'use strict';
    angular.module('app.account').component("contact", {
        templateUrl: "app/account/contact/contact-view.html",
        bindings: {
            contactData: '<',
            updateContact: '&'
        },
        controllerAs: "model",
        controller: function() {
            var vm = this
            console.log("Contact Controller")
            console.log(vm.contactData)
            vm.newContact =  angular.copy(vm.contactData.Account.Contact)

            


        }

    })
})();