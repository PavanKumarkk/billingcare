/**
 * Created by unni on 2016-05-18.
 */


(function() {
    'use strict';
    angular.module('app.account').component("accountHierarchy", {
        templateUrl: "app/account.hierarchy/account-hierarchy.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice, $scope) {
            var vm = this
            vm.$onInit = function() {
                dataservice.getHierarchyData(vm.accountData.Account.ID).then(function(data) {
                    console.log("getHierarchyData")
                    console.log(data)
                    //loop through billing data, set biling Grid Options

                    //vm.billingGridOptions = billingGridOptions
                    vm.acctTreeData = new kendo.data.HierarchicalDataSource({ data:[data]})

                })
            }
        }
    })
})();

