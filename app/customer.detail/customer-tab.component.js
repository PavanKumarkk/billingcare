/**
 * Created by unni on 2016-05-22.
 */


(function() {
    'use strict';
    angular.module('app.customer').component("customerTab", {

        template: "<div></div>",
        controllerAs: "model",
        require: {
            customerCtrl: '^customer'
        },
        controller: function(dataservice, $scope) {

            var vm = this

            vm.$onInit = function() {
                console.log("Init")

            }
            vm.$routerOnActivate = function(next, previous) {
                console.log(next)
                var id = next.params.id;
                vm.custID = id
                console.log("Router-ID:" + id)
                vm.customerCtrl.addCustomerTab(id)
            }
        }
    })
})();
