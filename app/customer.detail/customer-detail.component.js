
(function() {
    'use strict';
    angular.module('app.customer').component("customerDetail", {
        templateUrl: "app/customer.detail/customer.detail-view.html",
        bindings: {
            customerData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice, $scope, $timeout) {
            var vm = this
            vm.$onInit = function() {
                console.log("Customer Data")
                console.log(vm.customerData)
            }
            vm.tabs = [

            ];
            vm.accountData = [];
            vm.tabsdemo = [{text:"First Text"}, {},{},{}];
            vm.$routerOnActivate = function(next, previous) {

                console.log(next)


            }
            vm.$routerCanReuse = function() {
                return false
            }


            vm.openAccount = function(accountId) {

                var matchFound = false
                var currentTabIdx = 0

                for(var i = 0; i < vm.tabs.length; i++) {
                    if(vm.tabs[i].title == accountId) {
                        matchFound = true
                        //include the static tab and hence adding + 1
                        currentTabIdx = i + 1
                        break
                    }
                }
                if(!matchFound) {
                    dataservice.getAccountData(accountId).then(function(data) {
                        vm.currrentAccountID = accountId
                        vm.accountData[accountId] = data
                        vm.tabs.push({   title:  accountId,
                            content:"app/account/accountview.html"   })
                        currentTabIdx = vm.tabs.length
                        $timeout(function() {
                            $scope.active = currentTabIdx
                        })

                    })
                    //vm.tabs[0].active = "true"

                }
                console.log(vm.tabs.length)
                //apply it after updating the tabs



            }


        }
    })
})();