(function () {
    'use strict';
    angular.module('app.service').component("service", {
        templateUrl: "app/service/service-view.html",
        bindings: {
            accountData : '<'
        },
        controllerAs: "model",
        controller: function (dataservice, $scope,  $timeout) {
            var vm = this
            $scope.serviceTabStrip = null

            vm.tabs = [
                {
                    headerClass: "k-state-active", text: "Assets",
                    contentUrl: "app/service/service-assets-view.html", encoded: false
                }
            ]
            //$scope setting required for kendo
            $scope.tabs = vm.tabs

            //initialize static tag
            vm.$onInit = function() {
                dataservice.getServiceSummaryData(vm.accountData.Account.ID).then(function(data) {
                    vm.serviceSummary = data
                    $scope.serviceTabStrip.select(0)
                })
            }

            vm.viewServiceDetails = function(serviceID, primaryID) {
                console.log('ServiceID:' + serviceID)
                console.log('PrimaryID:' + primaryID)
                var matchFound = false
                var currentTabIdx  = 0


                for(var i = 0; i < vm.tabs.length; i++) {
                    if(vm.tabs[i].ServiceID == serviceID) {
                        matchFound = true
                        currentTabIdx = i
                        break
                    }
                }
                if(!matchFound) {
                    vm.currentServiceID = serviceID
                    vm.tabs.push({   text: primaryID,
                        contentUrl :"app/service/service-tab-view.html", encoded:false, closeBtn:true, ServiceID: serviceID })
                    currentTabIdx = vm.tabs.length -1
                }
                console.log($scope.serviceTabStrip)

                //execute the select at the end of digest cycle. This will allow the tabstrip to be updated by angular
                $timeout(function() {
                    $scope.serviceTabStrip.select(currentTabIdx);

                }, 1000)


            }
        }


    })
})();