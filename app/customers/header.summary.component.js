/**
 * Created by unni on 2016-05-15.
 */


(function() {
    'use strict';
    angular.module('app.customer').component("header", {
        templateUrl: "app/customers/headerview.html",
        controllerAs: "model",
        transclude: true,
        controller: function() {
            this.message = "Header1"
        }
    })
})();
