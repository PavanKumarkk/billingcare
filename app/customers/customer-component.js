/**
 * Created by unni on 2016-05-15.
 */


(function () {
    'use strict';
    angular.module('app.customer').component("customers", {
        templateUrl: "app/customers/customerview.html",
        controllerAs: "model",
        bindings: {
            $router: '<'
        },
        $routeConfig: [

            {path: "/", component: "customerList", name: "CustomerList", useAsDefault: true},
            {path: "/:id", component: "customerDetail", name: "CustomerDetail"}


        ],
        //$scope is required for Kendo Components..The values are being set in the
        // $scope..requirement for Kendo..Need to research
        controller: function ($scope) {
            var vm = this
            vm.customerTab = null
            vm.CustomerData = []

            vm.$routerCanReuse = function() {
                return true
            }
            vm.onShow = function () {
               // alert('on show')
            }

            vm.openCustomerTab = function (customerData) {
                console.log("In Open customer tab")
                vm.CustomerData.push(customerData)
                console.log(customerData)
                console.log(vm.customerTab)

                var headerTxt = accountData.Account.ID + " <button data-type='remove'>X</button>"
                $scope.customerTab.append({
                    headerClass: "k-state-active", text: headerTxt,
                    contentUrl: "app/customers/account-tabview.html", encoded: false
                })

                $scope.accountTab.select($scope.accountTab.tabGroup.length - 1)


            }
        }
    })
})();