/**
 * Created by unni on 2016-05-24.
 */


(function () {
    'use strict';
    angular.module('app.service').component("serviceDetails", {
        templateUrl: "app/service.details/service-details.view.html",
        bindings: {
            accountId : '@',
            serviceId: '@'
        },
        controllerAs: "model",
        controller: function (dataservice) {
            var vm = this

 
            vm.$onInit = function() {
                dataservice.getServiceDetailsData(vm.accountId, vm.serviceId).then(function(data) {
                    vm.serviceDetailsData = data
                }, function(data) {
                    console.log("Failed " + data)
                })
            }
        }


    })
})();