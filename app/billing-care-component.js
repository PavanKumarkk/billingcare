(function() {
    'use strict';
    angular.module('app.billingcare').component("billingCare", {
        templateUrl: "app/billing-care.html",
        //route names should begin with an uppercase letter. Route names should be CamelCase like "Dashboard".
        $routeConfig:[
            {path:"/dashboard", component:"dashboard", name:"Dashboard"},
            {path:"/customer/...", component:"customer", name:"Customer"},
            {path:"/search", component:"search", name:"Search"},
            {path:"/**", redirectTo: ["Dashboard"]}
        ],
        controllerAs: "model",
        controller: function($scope) {
            var vm = this

            vm.openSearch = function() {
                 $scope.searchWindow.center().open()
            }
        }

    })
})();