/**
 * Created by unni on 2016-05-21.
 */


(function() {
    'use strict';
    angular.module('app.customer').component("customerList", {
        templateUrl: "app/customer.list/customer-list.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this
            vm.$onInit = function() {


            }
            vm.$routerCanReuse = function() {
                return true
            }
            vm.$routerOnActivate = function(next, previous) {
                var id = next.params.id;

            }
        }
    })
})();