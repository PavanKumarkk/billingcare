/**
 * Created by unni on 2016-05-18.
 */

(function() {
    'use strict';
    angular.module('app.account').component("accountPayments", {
        templateUrl: "app/account.payments/account-payments.view.html",
        bindings: {
            accountData: '<'
        },
        controllerAs: "model",
        controller: function(dataservice) {
            var vm = this
            vm.$onInit = function() {
                dataservice.getPaymentData(vm.accountData.Account.ID).then(function(data) {
                    console.log("GetPaymentData")
                    console.log(data)
                    //loop through billing data, set biling Grid Options
                    vm.paymentData = data
                    //vm.billingGridOptions = billingGridOptions

                })
            }
        }
    })
})();
