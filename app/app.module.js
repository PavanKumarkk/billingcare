/**
 * Created by unni on 2016-05-16.
 */

(function () {
    'use strict';

    angular.module('myApp', [
        /*
         * Order is not important. Angular makes a
         * pass to register all of the modules listed
         * and then when app.dashboard tries to use app.data,
         * it's components are available.
         */

        /*
         * Everybody has access to these.
         * We could place these under every feature area,
         * but this is easier to maintain.
         */
        'app.core',
     /*   'app.data', // needs core*/
        'app.widgets', // needs core

        /*
         * Feature areas
         */
        'app.billingcare',
        'app.dashboard',
        'app.customer',
        'app.account',
        'app.search',
        'app.service',
        /**
         * Purely for testing..removing this will require a working backend server
         */
        "resourceMock"
    ]);

    angular.module('myApp').value("$routerRootComponent", "billingCare")

})();



