/**
 * Created by unni on 2016-05-22.
 */

(function () {
    'use strict';
    angular.module('app.customer').component("customer", {
        templateUrl: "app/customer/customer-view.html",
        controllerAs: "model",
        $routeConfig: [
            {path: "/", component: "customerList", name: "CustomerList", useAsDefault: true},
            {path: "/:id", component: "customerTab", name: "CustomerTab"}
        ],
        //$scope is required for Kendo Components..The values are being set in the
        // $scope..requirement for Kendo..Need to research
        controller: function ($scope, dataservice) {
            var vm = this
            console.log('Customer')
            $scope.customerTab = null
            vm.customerTabs = []

            vm.$onInit = function () {

            }
            vm.$routerOnActivate = function(next, previous) {
                console.log(next.params.id)

            }

            vm.$routerCanReuse = function() {
                return true
            }


             vm.addCustomerTab = function(customerID) {

                var headerTxt = 'CUST-' + customerID +  " <button data-type='remove'>X</button>"
                 console.log()

                if(vm.customerTabs[customerID]) {
                    console.log('tab already exists')
                } else {
                    dataservice.getCustomerData(customerID).then(function (data) {
                        vm.currentCustomerID = customerID
                        vm.customerTabs[customerID] = data
                        console.log(vm.customerTabs[customerID])
                        $scope.customerTab.append({
                            headerClass: "k-state-active", text: headerTxt,
                            contentUrl: "app/customer.detail/customer-tabview.html", encoded: false
                        })
                        $scope.customerTab.select($scope.customerTab.tabGroup.length - 1)

                    }, function(data) {
                        alert('failed')
                    })
                }

            }
        }
    })
})();
