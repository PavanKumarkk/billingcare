/**
 * Created by unni on 2016-04-28.
 */
/**
 * Created by unni on 2016-04-26.
 */
var app = angular.module('app', ['ui.bootstrap', 'kendo.directives','ngComponentRouter', 'heroes'])
    .config(function($locationProvider) {
        $locationProvider.html5Mode(true);
    }).value('$routerRootComponent', 'app')
    .component('app', {
        template: '<nav>\n' +
        '  <a>Crisis Center</a>\n' +
        '  <a ng-link="[\'Heroes\']">Heroes</a>\n' +
        '</nav>\n' +
        '<ng-outlet></ng-outlet>\n',
        $routeConfig: [
            {path: '/heroes/...', name: 'Heroes', component: 'heroes'},
        ]
    });

app.directive('kendoTabStrip', function($compile) {
    return {
        require: [
            '?kendoTabStrip'
        ],
        priority: 1,


        compile: function compile( tElement, tAttributes ) {
            console.log('In Compile' );
            return {
                pre: function preLink( scope, element, attributes, $document ) {
                    console.log(  ' (pre-link)'  );

                },
                post: function postLink(scope, element, attributes) {
                    console.log('post')
                    console.log("Scope")
                    console.log(scope)

                    var aElem = angular.element(element);
                    if(scope.tabs!= null) {
                        scope.tabs.forEach(function(tabData, index, array){
                            console.log(element)
                            if(tabData["closeBtn"]) {
                                console.log("Close Button requested")
                                var target = $(element[0]).find('.k-tabstrip-items > .k-item > .k-link')
                                console.log($(target[index]))
                                $(target[index]).append("<button data-type='remove'>X</button>")

                            }
                        })

                        aElem.data("kendoTabStrip").select(0)
                    }




                    aElem.on("click", "[data-type='remove']", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        var item = $(e.target).closest(".k-item");
                        console.log(item.index())

                        scope.$apply(function(){

                            scope.tabs.splice(item.index(), 1)

                           /* console.log(aElem.data("kendoTabStrip").tabGroup)
                            console.log(aElem.data("kendoTabStrip").remove(item.index()))
                            aElem.data("kendoTabStrip").select(0)
                            aElem.data("kendoTabStrip").reload()
                            */
                        })




                    });


                }
            };
        }





    }
})
app.controller('mainCtrl', function ($scope, $compile) {
    var currentTabIdx = 0
    var headerTemplateTxt = "<span>Item 2 <button data-type='remove' class='k-button k-button-icon'><span class='k-icon k-i-close'>" +
                "</span></button></span>"
    $scope.tabStripOptions1= {
        scrollable: false,
        tabPosition: "top",

    };
    $scope.tabStripOptions2 = {
        tabPosition: "top",

    };
    $scope.tabstripdemo = null;


    $scope.tabs = [
        { headerClass: "k-state-active", text: "Assets",
            contentUrl:"partials/asset_summary.html", encoded:false }

    ]

    $scope.viewServiceDetails = function(serviceId) {
        var matchFound = false

        for(i = 0; i < $scope.tabs.length; i++) {
            if($scope.tabs[i].text == serviceId) {
                matchFound = true
                currentTabIdx = i
                break
            }
        }
        if(!matchFound) {
            $scope.tabs.push({   text: serviceId,
                contentUrl:"partials/service_details.html", encoded:false, closeBtn:true })
            currentTabIdx = $scope.tabs.length -1
        }

        $scope.tabstripdemo.select(currentTabIdx);

    }


    $scope.$watch('tabstripdemo', function() {
       // $scope.tabstripdemo.append($scope.tabs[0])
     //   $scope.tabstripdemo.append($scope.tabs[1])

       $scope.tabstripdemo.select(currentTabIdx);



    });
        $scope.refreshTab = function() {
            $scope.tabs.push({   text: "mobileservice",
                content:"tabs2", encoded:false })




        }



    $scope.removeTab = function() {
        alert('remove')
    }


    $scope.productGridOptions = {
        dataSource: {
            data: productData,
            schema: {
                model: {
                    fields: {
                        PlanName:{ type: "string" },
                        BundleName: { type: "string" },
                        ProductName: { type: "string" },
                        RecurringFee: { type: "number" },
                        OnetimeFee: { type: "number" },
                        Status: { type: "string" },
                        PurchaseStartTime: { type: "string" },
                        PurchaseEndTime: { type: "string" }
                    }
                }
            },
            pageSize: 20
        },
        height: 250,
        scrollable: true,
        sortable: true,
        resizable: true,
        columnMenu: true,
        groupable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [

            { field: "PlanName", title: "Plan"  },
            { field: "BundleName", title: "Bundle"  },
            { field: "ProductName", title: "Product"  },
            { field: "RecurringFee", title: "RC", format: "{0:c}"  },
            { field: "OnetimeFee", title: "OC", format: "{0:c}"  },
            { field: "Status", title: "Status" },
            { field: "PurchaseStartTime",title: "PurchaseStart"  },
            { field: "PurchaseEndTime",title: "PurchaseEnd" }
        ]
    };

    $scope.productGridAddonOptions = {
        dataSource: {
            data: productAddonData,
            schema: {
                model: {
                    fields: {

                        BundleName: { type: "string" },
                        ProductName: { type: "string" },
                        RecurringFee: { type: "number" },
                        OnetimeFee: { type: "number" },
                        Status: { type: "string" },
                        PurchaseStartTime: { type: "string" },
                        PurchaseEndTime: { type: "string" }
                    }
                }
            },
            pageSize: 20
        },
        height: 250,
        scrollable: true,
        sortable: true,
        resizable: true,
        columnMenu: true,
        groupable: true,
        pageable: {
            input: true,
            numeric: false
        },
        columns: [


            { field: "BundleName", title: "Bundle"  },
            { field: "ProductName", title: "Product"  },
            { field: "RecurringFee", title: "RC", format: "{0:c}"  },
            { field: "OnetimeFee", title: "OC", format: "{0:c}"  },
            { field: "Status", title: "Status" },
            { field: "PurchaseStartTime",title: "PurchaseStart"  },
            { field: "PurchaseEndTime",title: "PurchaseEnd" }
        ]
    };


    $scope.mainGridOptions = {
        dataSource: {
            type: "odata",
            transport: {
                read: "//demos.telerik.com/kendo-ui/service/Northwind.svc/Employees"
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true
        },
        sortable: true,
        pageable: true,
        dataBound: function() {
            this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        columns: [{
            field: "FirstName",
            title: "First Name",
            width: "120px"
        },{
            field: "LastName",
            title: "Last Name",
            width: "120px"
        },{
            field: "Country",
            width: "120px"
        },{
            field: "City",
            width: "120px"
        },{
            field: "Title"
        }]
    };

    $scope.detailGridOptions = function(dataItem) {
        return {
            dataSource: {
                type: "odata",
                transport: {
                    read: "//demos.telerik.com/kendo-ui/service/Northwind.svc/Orders"
                },
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                pageSize: 5,
                filter: { field: "EmployeeID", operator: "eq", value: dataItem.EmployeeID }
            },
            scrollable: false,
            sortable: true,
            pageable: true,
            columns: [
                { field: "OrderID", title:"ID", width: "56px" },
                { field: "ShipCountry", title:"Ship Country", width: "110px" },
                { field: "ShipAddress", title:"Ship Address" },
                { field: "ShipName", title: "Ship Name", width: "190px" }
            ]
        };
    };

    $scope.source = new kendo.data.DataSource({
        data: servicedata,
        pageSize: 21
    });

    $scope.voiceBalance = 133
    $scope.dataBalance = 105
    $scope.textBalance = 250


})


app.controller("MyCtrl", function($scope){
        $scope.hello = "Hello from Controller!";
    })






