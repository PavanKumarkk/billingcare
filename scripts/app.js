/**
 * Created by unni on 2016-04-26.
 */
var app = angular.module('myApp', ['ui.bootstrap', 'kendo.directives','ngComponentRouter'])
    .config(function($locationProvider) {
        $locationProvider.html5Mode(true);
    })



app.controller('ModalControllerDemo', function($scope) {
    $scope.openModal = function() {
        $scope.modalOpen = true;
    }
    $scope.closeModal = function(response) {
        $scope.modalOpen = false;
        console.log('Modal closed', response)
    }
    $scope.modalClosed = function(response) {
        $scope.closeModal('no');
    }
});
app.controller('ModalController', function($scope) {
    $scope.close = function(response) {
        $scope.closeModal(response)
    }


});

app.controller('ModalCtrl', ['$scope',
    function($scope) {
        $scope.modalShown = false;
        $scope.toggleModal = function() {
            $scope.modalShown = !$scope.modalShown;
        };
    }
]);

app.directive('modal', function($document) {
    return {
        restrict: "E",
        scope: {
            modalOpen: '=open',
            options: '=',
            onClose: '&'
        },
        transclude: true,
        templateUrl: 'templates/modal.html',
        controller: function ($scope) {
           $scope.close = function() {
               $scope.modalOpen = false
               $scope.onClose()
           }
        },
        link:function ($scope, el, attrs) {
            var options =angular.extend( {
                height: '250px',
                width: '500px',
                top: '20%',
                left: '30%',
            }, $scope.options)

            el.find('.modal-container').css({
                'left' : options.left,
                'top' : options.top,
                'height' : options.height + 'px',
                'width' : options.width + 'px'
            })

            var pageHeight = $document.height();
            var pageWidth = $document.width();
            el.find('.modal-blackout').css({
                'width':pageWidth + 'px',
                'height':pageHeight + 'px'
            })
        }
    }

});



app.controller('mainCtrl', ['$scope',
    function($scope) {

        $scope.user = {
            name: "John Doe",
            address : {
                street : 'PO Box 123',
                city : 'Secret Rebel Base',
                planet : 'Yavin 4'
            },
            friends : [
                'Han',
                'Leia',
                'Chewbacca'
            ],
            hasForce: true,
            yearsOfJediTraining: 4,
            master: 'Yoda',
            passedTrials: true,
            masterApproves: true

        }

        $scope.user1 = {
            name: "Sam Adams",
            address : {
                street : 'PO Box 123',
                city : 'Secret Rebel Base',
                planet : 'Yavin 4'
            },
            friends : [
                'Han',
                'Leia',
                'Chewbacca'
            ]

        }
    }
]);


app.factory('jediPolicy', function($q) {
    return {
        advanceToKnight:function(candidate) {
            var promise = $q(function(resolve, reject) {
                if(candidate.hasForce && (candidate.yearsOfJediTraining > 20
                    || candidate.isChosenOne || (candidate.master==='Yoda'))) {
                    candidate.rank = "Jedi knight";
                    resolve(candidate)
                } else {
                    reject(candidate)
                }

            })
            return promise

        }
    }
});

//define camel case
//Bindings:
//SimpleValueParameter binding for collapsed..the user of the directive will pass-in collapsed attribute element and
//we want it be mapped to initialCollapsed. The incoming value will be string and we want that to be mapped to boolean

app.directive('userInfoCard', function(jediPolicy) {
    return {
        templateUrl: "templates/userinfocard.html",
        restrict: "E",
        scope: {
            user: '=',
            initialCollapsed: '@collapsed'
        },
        controller: function($scope, $uibModal) {

            $scope.collapsed = ($scope.initialCollapsed === 'true')
            $scope.knightMe = function( user ) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'templates/knightConfirmation.html',
                    controller: 'knightConfirmationCtrl',
                    resolve: {
                        user: function() {
                            return user
                        }
                    }

                })
                modalInstance.result.then(function(answer){
                    if(answer) {
                        jediPolicy.advanceToKnight(user).then(null, function(user) {
                            alert('not qualified')
                        })
                    }
                })


            };
            $scope.collapse = function() {

                $scope.collapsed = !$scope.collapsed
            }
            $scope.removeFriend = function(friend) {
                var $idx = $scope.user.friends.indexOf(friend)
                if($idx > -1) {
                    $scope.user.friends.splice($idx, 1)
                }
            }


        }
     }
});

app.controller('knightConfirmationCtrl', function($scope, $uibModalInstance, user){
    $scope.user= user
    $scope.yes = function() {
        $modalInstance.close(true)
    }

    $scope.no = function() {
        $modalInstance.close(false)
    }
})

app.directive('address', function() {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'templates/address.html',
        controller: function($scope) {
            $scope.collapsed = false;

            $scope.collapseAddress = function() {

                console.log($scope)
                $scope.collapsed = true;
            }
            $scope.expandAddress = function() {
                console.log($scope)
                $scope.collapsed = false;
            }





        }
    }
})
//method binding
app.directive('removeFriend', function() {
    return {
        restrict: 'E',
        templateUrl:'templates/removeFriend.html',
        scope: {
            notifyParent: '&method'
        },
        controller : function($scope) {
            $scope.removing = false;
            $scope.startRemove = function() {

                $scope.removing = true
            }
            $scope.cancelRemove = function() {
                $scope.removing = false
            }
            $scope.confirmRemove = function() {
                $scope.notifyParent()
            }



        }

    }
});

app.controller('decoratorCtrl', ['$scope',
    function($scope) {
        $scope.messages = [ ]
        $scope.handlePause = function(e) {
            console.log(e)
            $scope.messages.push({text: 'paused!'})
        }
    }
]);

app.directive('spacebarSupport', function() {
    return {
        restrict: "A",
        link: function(scope, el, attrs) {
            $('body').on('keypress', function(evt){
                var vidEl = el[0]
                if(evt.keyCode === 32) {

                   if(vidEl.paused)
                       vidEl.play()
                    else
                       vidEl.pause()
                }
            })
        }
    }
});

app.directive('eventPause', function($parse) {
    return {
        restrict: "A",

        link: function(scope, el, attrs) {
            var fn = $parse(attrs['eventPause']);
            //pause event happens outside of angular.
            //in order for angular to know about this event(this is required in cases where we need two way binding to be effective,
            //we need to trigger the event within the digest  cycle. In the below case, function eventPause which points to parent scope
            //handlepause function updates parent $scope.messages and prints out the messages. In order for angular to update the screen,
            //the object update should be part of the digest cycle
            el.on('pause', function(event) {
                scope.$apply(function() {
                    fn(scope, {evt:event})
                })

            })
        }
    }
});

app.controller('transcludeCtrl', ['$scope',
    function($scope) {

       $scope.items = [1,3, 6, 78]
    }
]);

app.directive('myTransclude', function() {
   return {
       restrict: 'E',
       transclude: true,
       template: '<div ng-transclude />'
   }
});